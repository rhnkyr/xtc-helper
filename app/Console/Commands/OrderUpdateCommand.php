<?php

namespace App\Console\Commands;

use App\Models\ShopwareOrder;
use App\Models\XtcOrder;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;

class OrderUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sw:order-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Syncs order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $xtc_orders = XtcOrder::with('products', 'totals')->orderByDesc('orders_id')->cursor();

        $vat_multiplier = 1;
        $sw_tax_id      = 6;
        $sw_tax_rate    = 0;
        $shipping       = 0;

        foreach ($xtc_orders as $xtc_order) {

            try {

                $threshold_date = Carbon::parse('2020-07-01 00:00:00');
                $date_purchased = Carbon::parse($xtc_order->date_purchased);

                if ($xtc_order->totals()->where('class', 'ot_tax')->exists()) {
                    if ($date_purchased->lte($threshold_date)) {
                        $vat_multiplier = 1.19;
                        $sw_tax_id      = 1;
                        $sw_tax_rate    = 19.00;
                    } else {
                        $vat_multiplier = 1.16;
                        $sw_tax_id      = 5;
                        $sw_tax_rate    = 16.00;
                    }
                }

                $sw_order = app(ShopwareOrder::class)
                    ->with('attribute', 'details')
                    ->where('ordernumber', $xtc_order->orders_id)
                    ->firstOrFail();

                if ($xtc_order->totals()->where('class', 'ot_total')->exists()) {
                    $total = $xtc_order->totals()->where('class', 'ot_total')->first()->value;
                } elseif ($xtc_order->totals()->where('class', 'ot_subtotal')->exists()) {
                    $total = $xtc_order->totals()->where('class', 'ot_subtotal')->first()->value;
                } else {
                    $total = $xtc_order->totals()->where('class', 'ot_subtotal_no_tax')->first()->value;
                }

                if ($xtc_order->totals()->where('class', 'ot_subtotal')->exists()) {
                    $net_total = $xtc_order->totals()->where('class', 'ot_subtotal')->first()->value;
                } else {
                    $net_total = $xtc_order->totals()->where('class', 'ot_subtotal_no_tax')->first()->value;
                }

                if ($xtc_order->totals()->where('class', 'ot_shipping')->exists()) {
                    $shipping = $xtc_order->totals()->where('class', 'ot_shipping')->first()->value;
                }

                $sw_order->remote_addr               = (string)$xtc_order->customers_ip; //IP
                $sw_order->ordertime                 = (string)$date_purchased; //Order Date
                $sw_order->invoice_amount            = round($total, 2);
                $sw_order->invoice_amount_net        = round($net_total, 2);
                $sw_order->invoice_shipping          = round($shipping * $vat_multiplier, 2);
                $sw_order->invoice_shipping_net      = round($shipping, 2);
                $sw_order->invoice_shipping_tax_rate = $sw_tax_rate;
                $sw_order->comment                   = '';
                $sw_order->internalcomment           = $xtc_order->comments;
                $sw_order->cleareddate               = $xtc_order->orders_date_paid;

                $sw_order->save();


                $sw_order->attribute()->updateOrCreate(
                    ['orderID' => $sw_order->id],
                    [
                        'where_did_find_the_product' => $xtc_order->woher,
                        'xtc_invoice_number'         => $xtc_order->bill_nr
                    ]
                );

                $prices = $xtc_order->products()->pluck('products_price', 'products_model');

                foreach ($sw_order->details()->get() as $detail) {
                    $detail->taxID    = $sw_tax_id;
                    $detail->tax_rate = $sw_tax_rate;

                    $detail->save();

                    $vat = "1.$sw_tax_rate";

                    $detail->price = (float)$prices[$detail->articleordernumber] * (float)$vat;

                    $detail->save();
                }

            } catch (ModelNotFoundException $exception) {
                Log::warning('Order not found on SW : ' . $xtc_order->orders_id);
                continue;
            } catch (\Exception $exception) {
                Log::error('Generic Order  : ' . $xtc_order->orders_id . ' Exception : ' . $exception->getMessage() . ' Line :' . $exception->getLine());
                continue;
            }

        }

        $this->info('Order Update Done');
        return 0;
    }
}
