<?php

namespace App\Console\Commands;

use App\Models\ShopwareProduct;
use Illuminate\Console\Command;

class ChangeProductOnSaleValue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sw:change-on-sale';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change value of : On Sale (If the stock is <= 0, the item is not available)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sw_products = ShopwareProduct::cursor();

        foreach ($sw_products as $product){
            $product->laststock = 1;
            $product->save();
        }

        $this->info('On Sale Update Done');
        return 0;
    }
}
