<?php

namespace App\Console\Commands;

use App\Models\ShopwareProductAttribute;
use App\Models\ShopwareProductDetail;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\LazyCollection;

class EkFekImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sw:ek-fek-import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'EK FEK Import';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        LazyCollection::make(function () {
            $filePath = storage_path('ek-fek-data.csv');
            $handle   = fopen($filePath, 'rb');
            while ($line = fgetcsv($handle)) {
                yield $line;
            }
        })
            ->chunk(1000)
            ->each(function ($lines) {
                foreach ($lines as $line) {

                    try {

                        $detail = app(ShopwareProductDetail::class)
                            ->where('ordernumber', $line[3])
                            ->firstOrFail();

                        app(ShopwareProductAttribute::class)
                            ->where('articledetailsID', $detail->articleID)
                            ->update([
                                'enet_article_xtcek'       => $line[0],
                                'enet_article_xtcfek'      => $line[1],
                                'enet_article_xtctest_fek' => $line[2],
                            ]);

                    } catch (ModelNotFoundException $exception) {
                        Log::error('Not found :' . $line[3]);
                        continue;
                    }

                }
            });

        $this->info('EK FEK Import Done');
        return 0;
    }
}
