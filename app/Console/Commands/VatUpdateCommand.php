<?php

namespace App\Console\Commands;

use App\Models\ShopwareCustomer;
use App\Models\XtcCustomer;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;

class VatUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sw:vat-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync the vat data from xt-commerce';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $xtc_customers = XtcCustomer::cursor()->filter(function ($xtc_customer) {
            return $xtc_customer->customers_vat_id !== '';
        });

        foreach ($xtc_customers as $xtc_customer) {

            try {

                $shopware_customer =
                    app(ShopwareCustomer::class)
                        ->with('addresses')
                        ->where('email', $xtc_customer->customers_email_address)
                        ->firstOrFail();

                $shopware_customer->addresses()->update([
                    'ustid' => str_replace(' ', '', $xtc_customer->customers_vat_id)
                ]);

            } catch (ModelNotFoundException $exception) {
                Log::error('Error on  shopware customer :' . $shopware_customer->email);
                continue;
            }
        }

        $this->info('Vat Update Done');
        return 0;
    }
}
