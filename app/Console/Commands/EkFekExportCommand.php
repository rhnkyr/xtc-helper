<?php

namespace App\Console\Commands;

use App\Models\XtcProduct;
use Illuminate\Console\Command;

class EkFekExportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'xt:ek-fek-export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'EK FEK Export';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $products = app(XtcProduct::class)->cursor();

        $file = fopen(storage_path('ek-fek-data.csv'), 'wb');

        foreach ($products as $product) {
            $row['EK']           = $product->products_ek;
            $row['FEK']          = $product->products_fek;
            $row['TEST_FEK']     = $product->products_test_fek;
            $row['PRODUCT_CODE'] = $product->products_model;

            fputcsv($file, [$row['EK'], $row['FEK'], $row['TEST_FEK'], $row['PRODUCT_CODE']]);
        }

        fclose($file);

        $this->info('EK FEK Export Done');

        return 0;
    }
}
