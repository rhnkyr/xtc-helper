<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RunAllCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sw:fix-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs all necessary commands.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Updates
        $this->call('sw:vat-update');
        $this->call('sw:fax-update');
        $this->call('sw:clean-address');
        $this->call('sw:clean-states');

        //Data exchange
        $this->call('xt:ek-fek-export');
        $this->call('sw:ek-fek-import');

        //Data fixes
        $this->call('sw:order-update');
        $this->call('sw:product-update');

        $this->info('All Done');
        return 0;
    }
}
