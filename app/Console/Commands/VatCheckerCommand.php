<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use SoapClient;
use SoapFault;

class VatCheckerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sw:check-ustid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check UST-Id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $vat_id = 'DE204524087';

        $vat_id = str_replace([' ', '.', '-', ',', ', '], '', trim($vat_id));
        $cc     = substr($vat_id, 0, 2);
        $vn     = substr($vat_id, 2);
        $client = new SoapClient("https://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl");

        if ($client) {
            $params = ['countryCode' => $cc, 'vatNumber' => $vn];
            try {
                $r = $client->checkVat($params);
                if ($r->valid === true) {
                    $this->info('VAT Valid');
                } else {
                    $this->warn('VAT not Valid');
                }

                foreach ($r as $k => $prop) {
                    $this->info('VAT ' . $k . ': ' . $prop);
                }

            } catch (SoapFault $e) {
                $this->error('Error, see message: ' . $e->faultstring);
            }
        } else {
            $this->error('Internal Error');
        }

        return 0;
    }
}
