<?php

namespace App\Console\Commands;

use App\Models\ShopwareCategory;
use App\Models\ShopwareRewriteUrl;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CategoryUrlCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sw:category-urls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populates category seo urls';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $categories = app(ShopwareCategory::class)
            ->whereNull('parent')
            ->with('childrenCategories')
            ->cursor();


        foreach ($categories as $category) {

            foreach ($category->childrenCategories as $childCategory) {

                $this->helper($childCategory);

            }

        }

        $this->info('Url Update Done');
        return 0;
    }

    private function helper($childCategory)
    {
        foreach ($childCategory->childrenCategories as $ccCategory) {
            $slug = Str::slug($childCategory->description) . '/' . Str::slug($ccCategory->description);

            app(ShopwareRewriteUrl::class)->insertOrIgnore(
                [
                    'org_path'  => "sViewport=cat&sCategory=$ccCategory->id",
                    'path'      => str_replace('deutsch/', '', $slug),
                    'main'      => 1, //Main means currently using url
                    'subshopID' => 1
                ]);

            $this->helper($ccCategory);
        }
    }
}
