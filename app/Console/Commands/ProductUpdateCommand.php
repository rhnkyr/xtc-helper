<?php

namespace App\Console\Commands;

use App\Models\ShopwareProductDetail;
use App\Models\ShopwareProductSupplier;
use App\Models\XtcProduct;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sw:product-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates product information';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        //Length fix of column
        $table = (new ShopwareProductDetail())->getTable();
        DB::connection('mysql_sw')->statement("ALTER TABLE $table MODIFY shippingtime varchar(50) NULL");

        $xtc_products = XtcProduct::with('manufacturer', 'shippingInfo', 'description')->cursor();

        try {

            $default_supplier = app(ShopwareProductSupplier::class)
                ->where('name', 'unbekannt')
                ->firstOrFail();

        } catch (ModelNotFoundException $exception) {

            $default_supplier = app(ShopwareProductSupplier::class)->create(
                [
                    'name'    => 'unbekannt',
                    'img'     => '',
                    'link'    => '',
                    'changed' => Carbon::now()
                ]
            );

        }

        foreach ($xtc_products as $xtc_product) {


            try {

                $sw_product_detail = app(ShopwareProductDetail::class)
                    ->with('product', 'attribute')
                    ->where('ordernumber', $xtc_product->products_model)
                    ->firstOrFail();

                if ($xtc_product->manufacturers_id === 0) {
                    $sw_product_detail->product()->update(
                        [
                            'supplierID' => $default_supplier->id
                        ]
                    );
                } else {

                    $xtc_supplier = $xtc_product->manufacturer;

                    $sw_supplier = app(ShopwareProductSupplier::class)
                        ->where('name', $xtc_supplier->manufacturers_name)
                        ->first();

                    $sw_product_detail->product()->update(
                        [
                            'supplierID'       => $sw_supplier->id,
                            'description'      => NULL,
                            'description_long' => $xtc_product->description->products_description
                        ]
                    );

                }

                $sw_product_detail->releasedate = NULL;

                $sw_product_detail->attribute()->update(
                    [
                        'enet_article_xtlagerort'                => $xtc_product->products_lagerort,
                        'enet_article_xtinternalweight'          => $xtc_product->internal_weight,
                        'enet_article_xtcid'                     => $xtc_product->products_id,
                        'enet_article_xtcondition'               => $xtc_product->products_zustand,
                        'enet_article_xtsperrigkeit'             => $xtc_product->products_sperrigkeit,
                        'enet_article_anfrage'                   => $xtc_product->ask_price,
                        'enet_article_xtcshort_description'      => $xtc_product->description->products_short_description,
                        'enet_article_xtproducts_begin_quantity' => $xtc_product->products_begin_quantitiy,
                        'enet_article_xtproducts_keywords'       => $xtc_product->description->products_keywords,
                        'enet_article_xtproducts_price_comment'  => $xtc_product->products_price_comment,
                    ]
                );

                $sw_product_detail->shippingtime = $xtc_product->shippingInfo->shipping_status_name;

                $sw_product_detail->save();
            } catch (ModelNotFoundException $exception) {
                Log::warning('Product not found on SW : ' . $xtc_product->products_model);
                continue;
            } catch (\Exception $exception) {
                Log::error('Generic Product  : ' . $xtc_product->products_model . ' Exception : ' . $exception->getMessage() . ' Line :' . $exception->getLine());
                continue;
            }

        }

        $this->info('Product Update Done');
        return 0;
    }
}
