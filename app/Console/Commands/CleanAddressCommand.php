<?php

namespace App\Console\Commands;

use App\Models\ShopwareCustomerAddress;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CleanAddressCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sw:clean-address';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans duplicated street addresses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $shop_customer_addresses = ShopwareCustomerAddress::cursor();

        foreach ($shop_customer_addresses as $shop_customer_address) {
            if (strpos($shop_customer_address->street, PHP_EOL) !== false) {
                $shop_customer_address->street = trim(preg_replace("/\r|\n/", ' ', $shop_customer_address->street));
                $shop_customer_address->save();
            }
        }

        $shop_customer_addresses = ShopwareCustomerAddress::cursor()->filter(function ($shop_customer_address) {
            return Str::slug(trim($shop_customer_address->street)) === Str::slug(trim($shop_customer_address->additional_address_line1))
                ||
                Str::slug(trim($shop_customer_address->street)) === Str::slug(trim($shop_customer_address->additional_address_line1) . trim($shop_customer_address->additional_address_line2))
                ||
                Str::slug(trim($shop_customer_address->street)) === Str::slug(trim($shop_customer_address->additional_address_line1) . ' ' . trim($shop_customer_address->additional_address_line2));
        });

        foreach ($shop_customer_addresses as $shop_customer_address) {

            try {

                $shop_customer_address->update(
                    [
                        'street'                   => $shop_customer_address->additional_address_line1 . ' ' . $shop_customer_address->additional_address_line2,
                        'additional_address_line1' => NULL,
                        'additional_address_line2' => NULL,
                    ]
                );

            } catch (ModelNotFoundException $exception) {
                Log::error('Error on  shopware customer :' . $shop_customer_address->user_id);
                continue;
            }
        }

        $this->info('Address Fixing Done');

        return 0;
    }
}
