<?php

namespace App\Console\Commands;

use App\Models\ShopwareCustomerAddress;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CleanAddressStateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sw:clean-states';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean up wrong state id from address';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $table = (new ShopwareCustomerAddress())->getTable();

        DB::connection('mysql_sw')->statement("UPDATE $table SET state_id = NULL");

        $this->info('State Cleanup Done');

        return 0;
    }
}
