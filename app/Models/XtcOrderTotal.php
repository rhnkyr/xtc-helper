<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class XtcOrderTotal extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $table = 'orders_total';
}
