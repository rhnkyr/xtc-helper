<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopwareCustomer extends Model
{
    use HasFactory;

    protected $connection = 'mysql_sw';
    protected $table = 's_user';

    public $timestamps = false;

    public function attributes()
    {
        return $this->hasMany(ShopwareCustomerAttribute::class, 'id', 'userID');
    }

    public function addresses()
    {
        return $this->hasMany(ShopwareCustomerAddress::class, 'user_id', 'id');
    }
}
