<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopwareOrderAttribute extends Model
{
    use HasFactory;

    protected $connection = 'mysql_sw';
    protected $table = 's_order_attributes';

    public $timestamps = false;

    protected $guarded = [];
}
