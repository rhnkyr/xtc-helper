<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopwareCustomerAddressAttribute extends Model
{
    use HasFactory;

    protected $connection = 'mysql_sw';
    protected $table = 's_user_addresses_attributes';
    protected $guarded = [];

    public $timestamps = false;
}
