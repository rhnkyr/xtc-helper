<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopwareCategory extends Model
{
    use HasFactory;

    protected $connection = 'mysql_sw';
    protected $table = 's_categories';

    public $timestamps = false;

    protected $guarded = [];

    public function categories()
    {
        return $this->hasMany(self::class, 'parent', 'id');
    }

    public function childrenCategories()
    {
        return $this->hasMany(self::class, 'parent', 'id')->with('categories');
    }
}
