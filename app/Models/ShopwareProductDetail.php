<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopwareProductDetail extends Model
{
    use HasFactory;

    protected $connection = 'mysql_sw';
    protected $table = 's_articles_details';

    public $timestamps = false;

    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(ShopwareProduct::class, 'articleID');
    }

    public function attribute()
    {
        return $this->hasOne(ShopwareProductAttribute::class, 'articledetailsID', 'articleID');
    }
}
