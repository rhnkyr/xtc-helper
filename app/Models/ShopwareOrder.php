<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopwareOrder extends Model
{
    use HasFactory;

    protected $connection = 'mysql_sw';
    protected $table = 's_order';

    public $timestamps = false;

    protected $guarded = [];

    public function attribute()
    {
        return $this->hasOne(ShopwareOrderAttribute::class, 'orderID');
    }

    public function details()
    {
        return $this->hasMany(ShopwareOrderDetail::class, 'orderID');
    }
}
