<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopwareProduct extends Model
{
    use HasFactory;

    protected $connection = 'mysql_sw';
    protected $table = 's_articles';

    public $timestamps = false;

    public function detail()
    {
        return $this->hasOne(ShopwareProductDetail::class, 'articleID');
    }
}
