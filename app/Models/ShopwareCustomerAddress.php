<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopwareCustomerAddress extends Model
{
    use HasFactory;

    protected $connection = 'mysql_sw';
    protected $table = 's_user_addresses';
    protected $guarded = [];

    public $timestamps = false;

    public function addressAttribute()
    {
        return $this->hasOne(ShopwareCustomerAddressAttribute::class, 'address_id', 'id');
    }
}
