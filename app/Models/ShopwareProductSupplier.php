<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopwareProductSupplier extends Model
{
    use HasFactory;

    protected $connection = 'mysql_sw';
    protected $table = 's_articles_supplier';

    public $timestamps = false;

    protected $guarded = [];
}
