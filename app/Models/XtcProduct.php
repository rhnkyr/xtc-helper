<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class XtcProduct extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $table = 'products';

    public function manufacturer()
    {
        return $this->hasOne(XtcSupplier::class, 'manufacturers_id', 'manufacturers_id');
    }

    public function shippingInfo()
    {
        return $this->hasOne(XtcShippingStatus::class, 'shipping_status_id', 'products_shippingtime');
    }

    public function description()
    {
        return $this->hasOne(XtcProductDescription::class, 'products_id', 'products_id');
    }
}
