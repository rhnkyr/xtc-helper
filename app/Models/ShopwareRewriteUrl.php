<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopwareRewriteUrl extends Model
{
    use HasFactory;

    protected $connection = 'mysql_sw';
    protected $table = 's_core_rewrite_urls';

    public $timestamps = false;

    protected $guarded = [];
}
