<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class XtcShippingStatus extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $table = 'shipping_status';
}
