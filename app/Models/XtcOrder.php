<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class XtcOrder extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $table = 'orders';

    public function products()
    {
        return $this->hasMany(XtcOrderProduct::class, 'orders_id', 'orders_id');
    }

    public function totals()
    {
        return $this->hasMany(XtcOrderTotal::class, 'orders_id', 'orders_id')->orderBy('sort_order', 'asc');
    }
}
