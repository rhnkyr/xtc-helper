<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopwareOrderDetail extends Model
{
    use HasFactory;

    protected $connection = 'mysql_sw';
    protected $table = 's_order_details';

    public $timestamps = false;

    protected $guarded = [];
}
