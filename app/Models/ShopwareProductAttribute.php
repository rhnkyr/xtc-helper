<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopwareProductAttribute extends Model
{
    use HasFactory;

    protected $connection = 'mysql_sw';
    protected $table = 's_articles_attributes';

    public $timestamps = false;
}
